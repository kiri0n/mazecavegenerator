# MazeCaveGenerator

## Description

The project was implemented in cooperation with @hmetallo

- In this project, the main algorithms for processing labyrinths and caves were implemented, including generation, rendering, and solving.

- The program was developed in the `C++` language of the `C++17` standard.

- Complied with `Google style`.

- The program is built using a Makefile, which contains the standard set of targets for GNU programs: all, install, remove, clean, dvi, dist, tests.

- GUI implementation based on GUI library with C++17 API: `Qt5`.

- The maximum size of the maze is 50x50

- The size of the maze cells themselves is calculated in such a way that the maze occupies the entire field allocated to it.

- The maze is generated using the `Eller algorithm`.
- Generating caves using a `cellular automaton`.

- The generated labyrinth has no isolations or loops.

- Full unit test coverage.

## Interface

![maze](/src/maze/documents/images/maze.png)
* Pic 1. Maze Generator

![cave](/src/maze/documents/images/cave.png)
* Pic 2. Cave generator.