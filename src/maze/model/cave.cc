#include "model/cave.h"

namespace s21 {

int Cave::GetRows() { return rows_; }
int Cave::GetCols() { return cols_; }
int Cave::GetBirth() { return birth_; }
int Cave::GetDeath() { return death_; }
bool Cave::GetCellValue(int row, int col) { return walls_[row][col]; }

void Cave::SetRows(int value) { rows_ = value; }
void Cave::SetCols(int value) { cols_ = value; }
void Cave::SetBirth(int value) { birth_ = value; }
void Cave::SetDeath(int value) { death_ = value; }

void Cave::SetData(int rows, int cols, WallsMatrix walls) {
  rows_ = rows;
  cols_ = cols;
  walls_ = walls;
  for (int i = 0; i < rows; ++i) {
    NeighborsVector neighbors_vec;
    for (int j = 0; j < cols; ++j) {
      neighbors_vec.push_back(CountAroundAliveCells(i, j));
    }
    neighbors_.push_back(neighbors_vec);
  }
}

void Cave::NextGeneration() {
  WallsMatrix new_walls;
  for (int i = 0; i < rows_; ++i) {
    WallsVector new_walls_vec;
    for (int j = 0; j < cols_; ++j) {
      new_walls_vec.push_back(CalculateCellNewValue(i, j));
    }
    new_walls.push_back(new_walls_vec);
  }
  walls_ = new_walls;
}

bool Cave::CalculateCellNewValue(int row, int col) {
  int count;
  count = CountAroundAliveCells(row, col);
  if (walls_[row][col] == true) {
    return (!(count < death_));
  } else {
    return (count > birth_);
  }
}

int Cave::CountAroundAliveCells(int row, int col) {
  int result = 0;
  for (int i = std::max(row - 1, 0); i <= std::min(row + 1, rows_ - 1); ++i) {
    for (int j = std::max(col - 1, 0); j <= std::min(col + 1, cols_ - 1); ++j) {
      if (!(i == row && j == col) && walls_[i][j] == true) ++result;
    }
  }
  return result;
}

void Cave::GenerateRandomWalls(double birth_chance) {
  walls_.clear();
  for (int i = 0; i < rows_; ++i) {
    WallsVector walls_vec;
    for (int j = 0; j < cols_; ++j) {
      walls_vec.push_back(RandomDouble() < birth_chance);
    }
    walls_.push_back(walls_vec);
  }
}

double Cave::RandomDouble() {
  static std::mt19937_64 engine(std::random_device{}());
  static std::uniform_real_distribution<double> distribution(0, 100);
  return distribution(engine);
}

void Cave::SaveFile(const std::string& filename) {
  std::ofstream outfile(filename);
  if (!outfile) {
    throw std::invalid_argument("Error opening file for writing.");
  }
  outfile << rows_ << " " << cols_ << "\n";
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      outfile << walls_[i][j] << " ";
    }
    outfile << "\n";
  }
  outfile.close();
}

}  // namespace s21
