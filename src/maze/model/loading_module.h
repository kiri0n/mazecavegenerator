#ifndef MAZE_MODEL_LOADING_MODULE_H_
#define MAZE_MODEL_LOADING_MODULE_H_

#include "model/cave.h"
#include "model/maze.h"

namespace s21 {

namespace data {

void LoadMatrix(std::ifstream &file, int rows, int cols, WallsMatrix &matrix);
void LoadMazeFromFile(Maze *&maze_ptr, const std::string &filename);
void LoadCaveFromFile(Cave *&cave_ptr, const std::string &filename);

}  // namespace data

}  // namespace s21

#endif  // MAZE_MODEL_LOADING_MODULE_H_
