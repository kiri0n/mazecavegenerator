#ifndef MAZE_MODEL_UTILS_H_
#define MAZE_MODEL_UTILS_H_

#include <queue>
#include <vector>

namespace s21 {

const int kCanvasSize = 500;
const int kDx[] = {1, 0, -1, 0};
const int kDy[] = {0, 1, 0, -1};
enum MoveDir { kRight, kUp, kLeft, kDown };
const MoveDir kAllDirs[] = {kRight, kUp, kLeft, kDown};

struct Point {
  int x;
  int y;
};

using PointsVector = std::vector<Point>;
using PointsQueue = std::queue<Point>;
using PointsMatrix = std::vector<PointsVector>;

using NeighborsVector = std::vector<int>;
using NeighborsMatrix = std::vector<NeighborsVector>;

using WallsVector = std::vector<bool>;
using WallsMatrix = std::vector<WallsVector>;

const int kEmpty = 0;

enum MapType { kMaze, kCave };

}  // namespace s21

#endif  // MAZE_MODEL_UTILS_H_
