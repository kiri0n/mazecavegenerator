#include "model/maze.h"

namespace s21 {

int Maze::GetRows() { return rows_; }
int Maze::GetCols() { return cols_; }

void Maze::SetRows(int value) { rows_ = value; }
void Maze::SetCols(int value) { cols_ = value; }

void Maze::SetData(int rows, int cols, WallsMatrix walls_right,
                   WallsMatrix walls_bottom) {
  rows_ = rows;
  cols_ = cols;
  walls_right_ = walls_right;
  walls_bottom_ = walls_bottom;
}

bool Maze::GetWallRightValue(int row, int col) {
  return walls_right_[row][col];
}

bool Maze::GetWallBottomValue(int row, int col) {
  return walls_bottom_[row][col];
}

void Maze::FillWithZero() {
  for (int i = 0; i < cols_; ++i) {
    unique_row_.push_back(kEmpty);
  }
}

void Maze::FillWithUnique() {
  for (int i = 0; i < cols_; ++i) {
    if (unique_row_[i] == kEmpty) {
      unique_row_[i] = serial_;
      serial_++;
    }
  }
}

void Maze::MergingCells(int index, int value) {
  if (index + 1 < cols_) {
    int temp_set = unique_row_[index + 1];
    for (int i = 0; i < cols_; ++i) {
      if (unique_row_[i] == temp_set) {
        unique_row_[i] = value;
      }
    }
  }
}

void Maze::CreateRightWall(int row) {
  for (int i = 0; i < cols_ - 1; ++i) {
    walls_right_[row][i] = RandomBool() || unique_row_[i] == unique_row_[i + 1];
    if (!walls_right_[row][i]) {
      MergingCells(i, unique_row_[i]);
    }
  }
}

int Maze::CountOfSet(int value) {
  int count_result = 0;
  for (int i = 0; i < cols_; ++i) {
    if (unique_row_[i] == value) {
      count_result++;
    }
  }
  return count_result;
}

void Maze::CheckBottomWall(int row) {
  for (int i = 0; i < cols_; ++i) {
    if (CountOfBottomWall(unique_row_[i], row) == 0) {
      walls_bottom_[row][i] = false;
    }
  }
}

int Maze::CountOfBottomWall(int value, int row) {
  int count_result = 0;
  for (int i = 0; i < cols_; ++i) {
    if (unique_row_[i] == value && walls_bottom_[row][i] == false) {
      count_result++;
    }
  }
  return count_result;
}

void Maze::PreparatingNewLine(int row) {
  for (int i = 0; i < cols_; ++i) {
    if (walls_bottom_[row][i] == true) {
      unique_row_[i] = kEmpty;
    }
  }
}

void Maze::CreateEndLine() {
  FillWithUnique();
  CreateRightWall(rows_ - 1);
  CheckEndLine();
}

void Maze::CheckEndLine() {
  for (int i = 0; i < cols_; ++i) {
    if (i + 1 < cols_ && unique_row_[i] != unique_row_[i + 1]) {
      walls_right_[rows_ - 1][i] = false;
      MergingCells(i, unique_row_[i]);
    }
    walls_bottom_[rows_ - 1][i] = true;
  }
}

void Maze::Reset() {
  unique_row_.clear();
  serial_ = 1;
}

void Maze::CreateBottomWall(int row) {
  for (int i = 0; i < cols_; ++i) {
    if (CountOfSet(unique_row_[i]) != 1) {
      walls_bottom_[row][i] = RandomBool();
    }
  }
}

bool Maze::RandomBool() {
  static std::mt19937_64 engine(std::random_device{}());
  static std::uniform_int_distribution<> distribution{0, 1};
  return static_cast<bool>(distribution(engine));
}

void Maze::Generate() {
  walls_right_ = WallsMatrix(rows_, WallsVector(cols_, false));
  walls_bottom_ = WallsMatrix(rows_, WallsVector(cols_, false));
  FillWithZero();
  for (int i = 0; i < rows_ - 1; ++i) {
    FillWithUnique();
    CreateRightWall(i);
    CreateBottomWall(i);
    CheckBottomWall(i);
    PreparatingNewLine(i);
  }
  CreateEndLine();
  FillLastCol();
  Reset();
}

void Maze::FillLastCol() {
  for (int i = 0; i < rows_; ++i) {
    walls_right_[i][cols_ - 1] = true;
  }
}

bool Maze::CanMove(int x, int y, MoveDir dir) {
  switch (dir) {
    case kRight:
      return x + 1 < cols_ && !walls_right_[y][x];
    case kUp:
      return y + 1 < rows_ && !walls_bottom_[y][x];
    case kLeft:
      return x - 1 >= 0 && !walls_right_[y][x - 1];
    case kDown:
      return y - 1 >= 0 && !walls_bottom_[y - 1][x];
    default:
      return false;
  }
}

PointsVector Maze::FindWay(Point start, Point end) {
  WallsMatrix visited(rows_, WallsVector(cols_));
  PointsMatrix parent(rows_, PointsVector(cols_));
  PointsQueue queue;

  visited[start.y][start.x] = true;
  parent[start.y][start.x] = {-1, -1};
  queue.push(start);

  bool found = false;

  while (!queue.empty() && !found) {
    Point current = queue.front();
    queue.pop();
    for (MoveDir dir : kAllDirs) {
      if (CanMove(current.x, current.y, dir)) {
        Point next = {current.x + kDx[dir], current.y + kDy[dir]};
        if (next.x == end.x && next.y == end.y) {
          found = true;
          parent[next.y][next.x] = current;
        }
        if (!visited[next.y][next.x]) {
          queue.push(next);
          visited[next.y][next.x] = true;
          parent[next.y][next.x] = current;
        }
      }
    }
  }

  PointsVector path;
  if (found) {
    Point current = end;
    while (current.x != -1 && current.y != -1) {
      path.push_back(current);
      current = parent[current.y][current.x];
    }
  }
  return path;
}

void Maze::SaveFile(const std::string& filename) {
  std::ofstream outfile(filename);
  if (!outfile) {
    throw std::invalid_argument("Error opening file for writing.");
  }
  outfile << rows_ << " " << cols_ << "\n";
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      outfile << walls_right_[i][j] << " ";
    }
    outfile << "\n";
  }
  outfile << "\n";
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      outfile << walls_bottom_[i][j] << " ";
    }
    outfile << "\n";
  }
  outfile.close();
}

}  // namespace s21
