#ifndef MAZE_MODEL_MAZE_H_
#define MAZE_MODEL_MAZE_H_

#include <fstream>
#include <random>

#include "model/map.h"
#include "model/utils.h"

namespace s21 {

class Maze : public Map {
 public:
  Maze(){};

  void SaveFile(const std::string& filename);
  void Generate();
  PointsVector FindWay(Point start, Point end);

  int GetRows();
  int GetCols();
  bool GetWallRightValue(int row, int col);
  bool GetWallBottomValue(int row, int col);

  void SetData(int rows, int cols, WallsMatrix walls_right,
               WallsMatrix walls_bottom);
  void SetRows(int value);
  void SetCols(int value);

 private:
  bool CanMove(int x, int y, MoveDir dir);
  void FillLastCol();
  void FillWithZero();
  void FillWithUnique();
  bool RandomBool();
  void MergingCells(int index, int value);
  void CreateRightWall(int rows);
  void CreateBottomWall(int rows);
  int CountOfSet(int value);
  void CheckBottomWall(int row);
  int CountOfBottomWall(int value, int row);
  void PreparatingNewLine(int row);
  void CreateEndLine();
  void CheckEndLine();
  void Reset();

  WallsMatrix walls_right_;
  WallsMatrix walls_bottom_;
  std::vector<int> unique_row_;
  int serial_ = 1;
};

}  // namespace s21

#endif  // MAZE_MODEL_MAZE_H_
