#include "model/loading_module.h"

namespace s21 {

namespace data {

void LoadMatrix(std::ifstream &file, int rows, int cols, WallsMatrix &matrix) {
  bool tmp;
  for (int i = 0; i < rows; i++) {
    WallsVector current;
    for (int j = 0; j < cols; j++) {
      file >> tmp;
      current.push_back(tmp);
    }
    matrix.push_back(current);
  }
}

void LoadMazeFromFile(Maze *&maze_ptr, const std::string &filename) {
  std::ifstream file(filename);
  if (file.is_open()) {
    int rows;
    file >> rows;
    int cols;
    file >> cols;
    WallsMatrix walls_right;
    LoadMatrix(file, rows, cols, walls_right);
    WallsMatrix walls_bottom;
    LoadMatrix(file, rows, cols, walls_bottom);
    file.close();
    Maze *new_maze = new Maze();
    new_maze->SetData(rows, cols, walls_right, walls_bottom);
    if (maze_ptr != nullptr) delete maze_ptr;
    maze_ptr = new_maze;
  }
}

void LoadCaveFromFile(Cave *&cave_ptr, const std::string &filename) {
  std::ifstream file(filename);
  if (file.is_open()) {
    int rows;
    file >> rows;
    int cols;
    file >> cols;
    WallsMatrix walls;
    LoadMatrix(file, rows, cols, walls);
    file.close();
    Cave *new_cave = new Cave();
    new_cave->SetData(rows, cols, walls);
    if (cave_ptr != nullptr) delete cave_ptr;
    cave_ptr = new_cave;
  }
}

}  // namespace data

}  // namespace s21
