#ifndef MAZE_MODEL_CAVE_H_
#define MAZE_MODEL_CAVE_H_

#include <fstream>
#include <random>

#include "model/map.h"
#include "model/utils.h"

namespace s21 {

class Cave : public Map {
 public:
  Cave(){};

  void SaveFile(const std::string& filename);
  void GenerateRandomWalls(double birth_chance);
  void NextGeneration();

  int GetRows();
  int GetCols();
  int GetBirth();
  int GetDeath();
  bool GetCellValue(int row, int col);

  void SetRows(int value);
  void SetCols(int value);
  void SetBirth(int value);
  void SetDeath(int value);
  void SetData(int rows, int cols, WallsMatrix walls);

 private:
  int CountAroundAliveCells(int row, int col);
  bool CalculateCellNewValue(int row, int col);
  double RandomDouble();

  int birth_ = 0;
  int death_ = 0;
  WallsMatrix walls_;
  NeighborsMatrix neighbors_;
};

}  // namespace s21

#endif  // MAZE_MODEL_CAVE_H_
