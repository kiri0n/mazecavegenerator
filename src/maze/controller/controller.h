#ifndef MAZE_CONTROLLER_CONTROLLER_H_
#define MAZE_CONTROLLER_CONTROLLER_H_

#include "model/cave.h"
#include "model/loading_module.h"
#include "model/maze.h"

namespace s21 {

class Controller {
 public:
  explicit Controller() {
    maze_ = new Maze();
    cave_ = new Cave();
  }

  Controller(const Controller &other) = delete;
  Controller &operator=(const Controller &other) = delete;

  ~Controller() {
    delete maze_;
    delete cave_;
  }

  void LoadMaze(const std::string &filename) {
    data::LoadMazeFromFile(maze_, filename);
  }

  void SaveMaze(const std::string &filename) { maze_->SaveFile(filename); }

  PointsVector FindWay(Point start, Point end) {
    return maze_->FindWay(start, end);
  }

  void GenerateMaze() { maze_->Generate(); }

  bool GetMazeWallRightValue(int row, int col) {
    return maze_->GetWallRightValue(row, col);
  }

  bool GetMazeWallBottomValue(int row, int col) {
    return maze_->GetWallBottomValue(row, col);
  }

  int GetMazeCols() { return maze_->GetCols(); }
  int GetMazeRows() { return maze_->GetRows(); }
  void SetMazeRows(int value) { maze_->SetRows(value); }
  void SetMazeCols(int value) { maze_->SetCols(value); }

  void LoadCave(const std::string &filename) {
    data::LoadCaveFromFile(cave_, filename);
  }

  void SaveCave(const std::string &filename) { cave_->SaveFile(filename); }

  void GenerateRandomWalls(double birth_chance) {
    cave_->GenerateRandomWalls(birth_chance);
  }

  void NextGeneration() { cave_->NextGeneration(); }
  int GetCaveCols() { return cave_->GetCols(); }
  int GetCaveRows() { return cave_->GetRows(); }
  bool GetCaveCellValue(int row, int col) {
    return cave_->GetCellValue(row, col);
  }
  int GetCaveBirth() { return cave_->GetBirth(); }
  int GetCaveDeath() { return cave_->GetDeath(); }
  void SetCaveRows(int value) { cave_->SetRows(value); }
  void SetCaveCols(int value) { cave_->SetCols(value); }
  void SetCaveBirth(int value) { cave_->SetBirth(value); }
  void SetCaveDeath(int value) { cave_->SetDeath(value); }

 private:
  Maze *maze_;
  Cave *cave_;
};

}  // namespace s21

#endif  // MAZE_CONTROLLER_CONTROLLER_H_
