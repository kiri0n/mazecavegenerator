#include <QApplication>

#include "view/main_window.h"

int main(int argc, char *argv[]) {
  setenv("LC_NUMERIC", "C", 1);
  QApplication a(argc, argv);
  s21::Controller ctrl;
  s21::View view(&ctrl);
  view.show();
  return a.exec();
}
