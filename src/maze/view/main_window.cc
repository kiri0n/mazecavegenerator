#include "main_window.h"

namespace s21 {

View::View(Controller* ctrl, QWidget* parent)
    : QMainWindow(parent), ctrl_(ctrl), ui_(new Ui::MainWindow) {
  ui_->setupUi(this);
  painter_ = new Painter(ctrl);
  ui_->horizontalLayout_3->addWidget(painter_);
  painter_->setEnabled(true);
  painter_->setVisible(true);
  ui_->chance_doubleSpinBox->setLocale(QLocale::c());
  connect(timer_, SIGNAL(timeout()), this, SLOT(CaveNextGeneration()));
}

View::~View() { delete ui_; }

void View::on_load_maze_pushButton_clicked() { Load(kMaze); }

void View::on_load_cave_pushButton_clicked() {
  Load(kCave);
  ui_->y_cave_spinBox->setValue(ctrl_->GetCaveCols());
  ui_->x_cave_spinBox->setValue(ctrl_->GetCaveRows());
}

void View::Load(MapType type) {
  QString filter = "TXT Files (*.txt)";
  QString filename = QFileDialog::getOpenFileName(this, "Select a file",
                                                  QDir::homePath(), filter);
  if (filename.isEmpty() == false) {
    if (type == kMaze) {
      try {
        ctrl_->LoadMaze(filename.toStdString());

      } catch (const std::invalid_argument& e) {
        QString error_msg = QString::fromStdString(e.what());
        QMessageBox::critical(this, "Error", error_msg, QMessageBox::Ok);
      }
    } else {
      try {
        ctrl_->LoadCave(filename.toStdString());

      } catch (const std::invalid_argument& e) {
        QString error_msg = QString::fromStdString(e.what());
        QMessageBox::critical(this, "Error", error_msg, QMessageBox::Ok);
      }
    }
    painter_->update();
  }
}

void View::on_generate_maze_pushButton_clicked() {
  ctrl_->SetMazeCols(ui_->y_spinBox->value());
  ctrl_->SetMazeRows(ui_->x_spinBox->value());
  ctrl_->GenerateMaze();
  painter_->UnsetPoints();
  painter_->update();
}

void View::on_auto_cave_pushButton_clicked() {
  auto_mode_ = !auto_mode_;
  if (auto_mode_) {
    timer_->start(ui_->delay_spinBox->text().toInt());
    ui_->auto_cave_pushButton->setText(QString("Stop auto"));
  } else {
    timer_->stop();
    ui_->auto_cave_pushButton->setText(QString("Auto"));
  }
}

void View::CaveNextGeneration() {
  ctrl_->NextGeneration();
  painter_->update();
}

void View::on_generate_cave_pushButton_clicked() {
  ctrl_->SetCaveDeath(ui_->death_spinBox->text().toInt());
  ctrl_->SetCaveBirth(ui_->birth_spinBox->text().toInt());
  ctrl_->SetCaveCols(ui_->y_cave_spinBox->text().toInt());
  ctrl_->SetCaveRows(ui_->x_cave_spinBox->text().toInt());
  ctrl_->GenerateRandomWalls(ui_->chance_doubleSpinBox->text().toDouble());
  painter_->update();
}

void View::on_next_cave_pushButton_clicked() {
  ctrl_->SetCaveDeath(ui_->death_spinBox->text().toInt());
  ctrl_->SetCaveBirth(ui_->birth_spinBox->text().toInt());
  int cols = ui_->y_cave_spinBox->text().toInt();
  int rows = ui_->x_cave_spinBox->text().toInt();
  if (cols <= ctrl_->GetCaveCols()) {
    ctrl_->SetCaveCols(cols);
  }
  if (rows <= ctrl_->GetCaveRows()) {
    ctrl_->SetCaveRows(rows);
  }
  ctrl_->NextGeneration();
  painter_->update();
}

void View::on_save_cave_pushButton_clicked() {
  QString filename =
      QFileDialog::getSaveFileName(this, "Save cave", ".", "*.txt");
  if (!filename.isEmpty()) {
    try {
      ctrl_->SaveCave(filename.toStdString());
    } catch (const std::invalid_argument& e) {
      QString error_msg = QString::fromStdString(e.what());
      QMessageBox::critical(this, "Error", error_msg, QMessageBox::Ok);
    }
  }
}

void View::on_save_maze_pushButton_clicked() {
  QString filename =
      QFileDialog::getSaveFileName(this, "Save maze", ".", "*.txt");
  if (!filename.isEmpty()) {
    try {
      ctrl_->SaveMaze(filename.toStdString());
    } catch (const std::invalid_argument& e) {
      QString error_msg = QString::fromStdString(e.what());
      QMessageBox::critical(this, "Error", error_msg, QMessageBox::Ok);
    }
  }
}

void View::on_tabWidget_currentChanged(int tab_index) {
  if (tab_index == 1) {
    painter_->SetActive(s21::kCave);
  } else {
    painter_->SetActive(s21::kMaze);
  }
  painter_->update();
}

}  // namespace s21
