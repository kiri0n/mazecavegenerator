#ifndef MAZE_VIEW_MAIN_WINDOW_H_
#define MAZE_VIEW_MAIN_WINDOW_H_

#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>

#include "controller/controller.h"
#include "view/painter.h"
#include "view/ui_main_window.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

namespace s21 {

class View : public QMainWindow {
  Q_OBJECT

 public:
  explicit View(Controller *controller, QWidget *parent = nullptr);
  ~View();

 private:
  Ui::MainWindow *ui_;
  Controller *ctrl_;
  Painter *painter_;
  bool auto_mode_ = false;
  QTimer *timer_ = new QTimer();
  void Load(MapType type);

 private slots:
  void CaveNextGeneration();

  void on_load_maze_pushButton_clicked();
  void on_load_cave_pushButton_clicked();
  void on_save_cave_pushButton_clicked();
  void on_save_maze_pushButton_clicked();
  void on_generate_maze_pushButton_clicked();
  void on_generate_cave_pushButton_clicked();
  void on_next_cave_pushButton_clicked();
  void on_auto_cave_pushButton_clicked();
  void on_tabWidget_currentChanged(int tab_index);
};

}  // namespace s21

#endif  // MAZE_VIEW_MAIN_WINDOW_H_
