#include "painter.h"

namespace s21 {

Painter::Painter(Controller *ctrl, QWidget *parent)
    : QWidget(parent), ctrl_(ctrl), ui_(new Ui::Painter) {
  ui_->setupUi(this);
  setEnabled(true);
  setVisible(true);
}

Painter::~Painter() { delete ui_; }

void Painter::SetActive(MapType value) { active_map_ = value; }

void Painter::UnsetPoints() {
  start_point_ = {0, 0};
  end_point_ = {0, 0};
}

void Painter::paintEvent(QPaintEvent *event) {
  Q_UNUSED(event);
  if (active_map_ == kMaze) {
    DrawMaze();
  } else {
    DrawCave();
  }
}

void Painter::DrawMaze() {
  int cols = ctrl_->GetMazeCols();
  int rows = ctrl_->GetMazeRows();
  if (rows > 1 && cols > 1) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    pen.setColor(QColor(252, 151, 0));
    pen.setWidth(2);
    painter.setPen(pen);
    int cell = kCanvasSize / std::max(rows, cols);
    int y_0 = (kCanvasSize - cell * rows) / 2;
    int x_0 = (kCanvasSize - cell * cols) / 2;
    painter.drawRect(x_0, y_0, cell * cols, cell * rows);
    PointsVector points;
    if (!(start_point_.x == end_point_.x && start_point_.y == end_point_.y)) {
      points = ctrl_->FindWay(start_point_, end_point_);
    }

    for (int i = 0; i < rows; ++i) {
      for (int j = 0; j < cols; ++j) {
        if (ctrl_->GetMazeWallBottomValue(i, j)) {
          int x = j * cell;
          int y = i * cell + cell;
          painter.drawLine(x_0 + x, y_0 + y, x_0 + x + cell, y_0 + y);
        }
        if (ctrl_->GetMazeWallRightValue(i, j)) {
          int x = j * cell + cell;
          int y = i * cell;
          painter.drawLine(x_0 + x, y_0 + y, x_0 + x, y_0 + y + cell);
        }
      }
    }

    pen.setColor(QColorConstants::White);
    painter.setPen(pen);
    for (int i = 1; i < points.size(); ++i) {
      int x_prev = points[i - 1].x * cell + cell / 2;
      int y_prev = points[i - 1].y * cell + cell / 2;
      int x = points[i].x * cell + cell / 2;
      int y = points[i].y * cell + cell / 2;
      painter.drawLine(x_0 + x_prev, y_0 + y_prev, x_0 + x, y_0 + y);
    }
    painter.end();
  }
}

void Painter::mousePressEvent(QMouseEvent *event) {
  int cols = ctrl_->GetMazeCols();
  int rows = ctrl_->GetMazeRows();
  if (active_map_ == kMaze && rows > 1 && cols > 1) {
    int cell = kCanvasSize / std::max(cols, rows);
    int y_0 = (kCanvasSize - cell * rows) / 2;
    int x_0 = (kCanvasSize - cell * cols) / 2;
    if (event->button() == Qt::LeftButton) {
      start_point_.x = (event->position().x() - x_0) / cell;
      start_point_.y = (event->position().y() - y_0) / cell;
    } else if (event->button() == Qt::RightButton) {
      end_point_.x = (event->position().x() - x_0) / cell;
      end_point_.y = (event->position().y() - y_0) / cell;
    }
  }
  QWidget::mousePressEvent(event);
  update();
}

void Painter::DrawCave() {
  int rows = ctrl_->GetCaveRows();
  int cols = ctrl_->GetCaveCols();
  if (rows > 0 && cols > 0) {
    QPainter painter(this);
    QBrush fill_brush(QColor(252, 151, 0));
    QPen outline_pen(Qt::NoPen);
    painter.setBrush(fill_brush);
    painter.setPen(outline_pen);
    int cell = kCanvasSize / std::max(rows, cols);

    for (int i = 0; i < rows; ++i) {
      for (int j = 0; j < cols; ++j) {
        if (ctrl_->GetCaveCellValue(i, j)) {
          painter.drawRect(j * cell, i * cell, cell, cell);
        }
      }
    }
  }
}

}  // namespace s21
