#ifndef MAZE_VIEW_PAINTER_H_
#define MAZE_VIEW_PAINTER_H_

#include <QMouseEvent>
#include <QPainter>
#include <QWidget>

#include "../controller/controller.h"
#include "ui_painter.h"

namespace Ui {
class Painter;
}

namespace s21 {

class Painter : public QWidget {
  Q_OBJECT

 public:
  explicit Painter(Controller *ctrl, QWidget *parent = nullptr);
  ~Painter();
  void SetActive(MapType value);
  void UnsetPoints();

 protected:
  void paintEvent(QPaintEvent *event) override;
  void mousePressEvent(QMouseEvent *event) override;

 private:
  void DrawCave();
  void DrawMaze();

  Ui::Painter *ui_;
  Controller *ctrl_;
  MapType active_map_ = kMaze;
  Point start_point_ = {0, 0};
  Point end_point_ = {0, 0};
};

}  // namespace s21

#endif  // MAZE_VIEW_PAINTER_H_
